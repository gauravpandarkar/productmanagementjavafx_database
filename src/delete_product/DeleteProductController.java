package delete_product;

import db_operations.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import show_options.ShowOptions;

public class DeleteProductController {
	@FXML
	private TextField productName;
	@FXML
	private Button back;
	@FXML
	private Button delete;
	@FXML
	private Button next;

	public void deleteProduct(ActionEvent event) throws Exception {
		System.out.println(productName.getText());

		String query = " delete from Product where Name ='" + productName.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		new ShowOptions().show();

	}

}
